/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.ejb.beans;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import pl.sda.ejb.model.Book;

/**
 *
 * @author artisticImpresion
 */
@Stateless
@LocalBean
public class NewSessionBean implements NewSessionBeanLocal {

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
//    private final ArrayList<String> books; już niepotrzebne
    
    @PersistenceContext(unitName = "persistence_unit")
    private EntityManager em;
    
    public NewSessionBean() {

    }
    
    @Override
    public List<Book> getBooks() {
        Query query = em.createQuery("from Book");
        //zapytanie można też zapisać "select c from Book c"
        return query.getResultList();
    }
    
    @Override
    public void addBook(String bookName) {
        Book book = new Book();
        book.setIsbn("1113567890");
        book.setTitle(bookName);
        book.setReleaseDate(1999);
        //EntityTransaction transaction = em.getTransaction();
        em.persist(book);
        //transaction.commit();
    }
      
    
}
