/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.ejb.beans;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import pl.sda.ejb.model.Author;
import pl.sda.ejb.model.Book;

/**
 *
 * @author artisticImpresion
 */
@Stateless
public class BookBean implements BookBeanIfc {

    @PersistenceContext(unitName = "persistence_unit")
    private EntityManager em;

    @Override
    public Book addBook(Book book) {
        if (book != null) {
            em.persist(book);
            return book;
        }
        return null;
    }

    @Override
    public Author addAuthor(Author author) {
        if (author != null) {
            em.persist(author);
            return author;
        }
        return null;
    }

    @Override
    public List<Book> getBooks() {
        Query createQuery = em.createQuery("from Book b");
        return createQuery.getResultList();

    }
}
