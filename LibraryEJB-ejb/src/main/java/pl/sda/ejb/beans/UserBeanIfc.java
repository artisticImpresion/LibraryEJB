/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.ejb.beans;

import javax.ejb.Local;
import javax.ejb.Remote;
import pl.sda.ejb.model.Book;
import pl.sda.ejb.model.User;

/**
 *
 * @author artisticImpresion
 */
@Local
@Remote
public interface UserBeanIfc {
    
    public User createUser(User user);
    public Book rentBook(Integer userId, Integer bookId);
    
}
