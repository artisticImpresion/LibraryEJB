/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.ejb.beans;

import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import pl.sda.ejb.model.Author;
import pl.sda.ejb.model.Book;

/**
 *
 * @author artisticImpresion
 */
@Local
@Remote
public interface BookBeanIfc {

    Book addBook(Book book);

    Author addAuthor(Author author);

    public List<Book> getBooks();

}
