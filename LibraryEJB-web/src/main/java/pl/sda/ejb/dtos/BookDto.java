/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.ejb.dtos;

import java.util.Collection;

/**
 *
 * @author artisticImpresion
 */
public class BookDto {
    private Integer id;
    private String bookTtitle;
    private String bookIsbn;
    private int bookReleaseDate;
    private Collection<AuthorDto> authors;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBookTtitle() {
        return bookTtitle;
    }

    public void setBookTtitle(String bookTtitle) {
        this.bookTtitle = bookTtitle;
    }

    public String getBookIsbn() {
        return bookIsbn;
    }

    public void setBookIsbn(String bookIsbn) {
        this.bookIsbn = bookIsbn;
    }

    public int getBookReleaseDate() {
        return bookReleaseDate;
    }

    public void setBookReleaseDate(int bookReleaseDate) {
        this.bookReleaseDate = bookReleaseDate;
    }

    public Collection<AuthorDto> getAuthors() {
        return authors;
    }

    public void setAuthors(Collection<AuthorDto> authors) {
        this.authors = authors;
    }
    
}

