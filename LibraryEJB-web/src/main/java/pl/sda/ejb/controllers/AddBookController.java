/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.ejb.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import pl.sda.ejb.beans.BookBeanIfc;
import pl.sda.ejb.model.Author;
import pl.sda.ejb.model.Book;


/**
 *
 * @author artisticImpresion
 */
@ManagedBean(name = "addBookController")
@RequestScoped
public class AddBookController implements Serializable {
    
    @EJB
    private BookBeanIfc bookBean;
    
    private String title;
    private String isbn;
    private int releaseDate;
    private String authors;

    /**
     * Creates a new instance of AddBookController
     */
    public AddBookController() {
    }
    
    public void save() {
      try {
          Book book = new Book();
          book.setTitle(title);
          book.setIsbn(isbn);
          book.setReleaseDate(releaseDate);
          
          String[] names = authors.split(",");
          ArrayList<Author> booksAuthors = new ArrayList<>();
          for (String name : names) {
              Author author = new Author();
              author.setName(name);
              ArrayList<Book> arrayList = new ArrayList<Book>();
              arrayList.add(book);
              author.setBooks(arrayList);
              booksAuthors.add(author);
          }
          
          book.setAuthors(booksAuthors);
          bookBean.addBook(book);
      } catch (Exception ex) {
          ex.printStackTrace();
      }
    }
    
   
        public String getText() {
        return "dodano książkę";
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public int getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(int releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }
    
        
}
