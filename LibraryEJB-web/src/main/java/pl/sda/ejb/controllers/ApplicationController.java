/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.ejb.controllers;

import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import pl.sda.ejb.beans.NewSessionBeanLocal;


/**
 *
 * @author artisticImpresion
 */
@Named(value = "applicationController")
@ApplicationScoped
public class ApplicationController {
    
    
    @EJB
    private NewSessionBeanLocal bean;

    /**
     * Creates a new instance of ApplicationController
     */
    public ApplicationController() {
    }
    
    public String getText() {
        //bean.addBook("Cień w ukryciu");
        return "test";
    }
    
}
