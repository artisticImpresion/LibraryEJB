/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.ejb.controllers;

import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import pl.sda.ejb.beans.BookBeanIfc;
import pl.sda.ejb.dtos.BookDto;
import pl.sda.ejb.model.Book;
import org.modelmapper.ModelMapper;

/**
 *
 * @author artisticImpresion
 */
@ManagedBean(name = "bookTableController")
@RequestScoped
public class BookTableController {

    @EJB
    private BookBeanIfc bookBeanI;

    /**
     * Creates a new instance of BookTableController
     */
    public BookTableController() {
    }
    // metoda getList() pochodzi z mappera ?

    public List<BookDto> getList() {
        List<Book> books = bookBeanI.getBooks();
        List<BookDto> collect = books.stream().map(this::map).collect(Collectors.toList());
        return collect;
    }

    private BookDto map(Book b) {
        ModelMapper mapper = new ModelMapper();
        BookDto map = mapper.map(b, BookDto.class);
        return map;
    }

}
